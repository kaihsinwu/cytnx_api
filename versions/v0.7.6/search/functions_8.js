var searchData=
[
  ['hadamard',['hadamard',['../namespacecytnx_1_1qgates.html#a37aadd0c317df789f26bad99652d0d3e',1,'cytnx::qgates']]],
  ['histogram',['Histogram',['../classcytnx_1_1stat_1_1Histogram.html#a352c96dc863dbe89ed3cd93d9578cf28',1,'cytnx::stat::Histogram']]],
  ['histogram2d',['Histogram2d',['../classcytnx_1_1stat_1_1Histogram2d.html#a9c049e4ebca66b565db0e7d2b133d50c',1,'cytnx::stat::Histogram2d']]],
  ['hosvd',['Hosvd',['../namespacecytnx_1_1linalg.html#ae148b196f8bd8b79b6ccb8855d4e13ea',1,'cytnx::linalg::Hosvd(const cytnx::UniTensor &amp;Tin, const std::vector&lt; cytnx_uint64 &gt; &amp;mode, const bool &amp;is_core=true, const bool &amp;is_Ls=false, const std::vector&lt; cytnx_int64 &gt; &amp;trucate_dim=std::vector&lt; cytnx_int64 &gt;())'],['../namespacecytnx_1_1linalg.html#a99631dd49eb552eaff1c40a54a1e568d',1,'cytnx::linalg::Hosvd(const Tensor &amp;Tin, const std::vector&lt; cytnx_uint64 &gt; &amp;mode, const bool &amp;is_core=true, const bool &amp;is_Ls=false, const std::vector&lt; cytnx_int64 &gt; &amp;trucate_dim=std::vector&lt; cytnx_int64 &gt;())']]]
];
