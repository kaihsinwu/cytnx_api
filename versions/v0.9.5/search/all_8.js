var searchData=
[
  ['has_5fduplicate_5fqnums_0',['has_duplicate_qnums',['../classcytnx_1_1Bond.html#acd82fa191fc9a25f12ae37899a3aea1e',1,'cytnx::Bond']]],
  ['hbar_1',['hBar',['../Physics_8hpp.html#adafb6f190ca6cd537bf81bb83b86fdca',1,'Physics.hpp']]],
  ['histogram_2',['Histogram',['../classcytnx_1_1stat_1_1Histogram.html',1,'cytnx::stat::Histogram'],['../classcytnx_1_1stat_1_1Histogram.html#a352c96dc863dbe89ed3cd93d9578cf28',1,'cytnx::stat::Histogram::Histogram()']]],
  ['histogram2d_3',['Histogram2d',['../classcytnx_1_1stat_1_1Histogram2d.html',1,'cytnx::stat::Histogram2d'],['../classcytnx_1_1stat_1_1Histogram2d.html#a9c049e4ebca66b565db0e7d2b133d50c',1,'cytnx::stat::Histogram2d::Histogram2d()']]],
  ['hosvd_4',['Hosvd',['../namespacecytnx_1_1linalg.html#aa1a08b9294b199a6d56f5f5aa2fa58cc',1,'cytnx::linalg::Hosvd(const cytnx::UniTensor &amp;Tin, const std::vector&lt; cytnx_uint64 &gt; &amp;mode, const bool &amp;is_core=true, const bool &amp;is_Ls=false, const std::vector&lt; cytnx_int64 &gt; &amp;trucate_dim=std::vector&lt; cytnx_int64 &gt;())'],['../namespacecytnx_1_1linalg.html#a6c3142f633f9aca2f80bb359b9c58cf5',1,'cytnx::linalg::Hosvd(const Tensor &amp;Tin, const std::vector&lt; cytnx_uint64 &gt; &amp;mode, const bool &amp;is_core=true, const bool &amp;is_Ls=false, const std::vector&lt; cytnx_int64 &gt; &amp;trucate_dim=std::vector&lt; cytnx_int64 &gt;())']]],
  ['hplanck_5',['hPlanck',['../Physics_8hpp.html#acb991e9f5a4457fb08f1d844165d4042',1,'Physics.hpp']]],
  ['hsplit_6',['Hsplit',['../namespacecytnx_1_1algo.html#a882e6a0d33e659d35f7f1df18a51152a',1,'cytnx::algo']]],
  ['hsplit_5f_7',['Hsplit_',['../namespacecytnx_1_1algo.html#a030124e0b8e894d35837134e3111e3c7',1,'cytnx::algo']]],
  ['hstack_8',['Hstack',['../namespacecytnx_1_1algo.html#aab9f9b299fa60c565206f5f0c021618b',1,'cytnx::algo']]]
];
