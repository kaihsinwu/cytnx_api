var searchData=
[
  ['qdr_0',['Qdr',['../namespacecytnx_1_1linalg.html#ac3a4d9f746e4d05555de50ac9fca9a77',1,'cytnx::linalg::Qdr(const cytnx::UniTensor &amp;Tin, const bool &amp;is_tau=false)'],['../namespacecytnx_1_1linalg.html#a49f6c1a406ac0a3446ea8c9423587dd8',1,'cytnx::linalg::Qdr(const Tensor &amp;Tin, const bool &amp;is_tau=false)']]],
  ['qns_1',['qns',['../classcytnx_1_1Accessor.html#a7d0a4717c759186aa48daa0ac0dab5c4',1,'cytnx::Accessor']]],
  ['qnums_2',['qnums',['../classcytnx_1_1Bond.html#ab4f05193a012bd9a077374b5f305e898',1,'cytnx::Bond::qnums() const'],['../classcytnx_1_1Bond.html#a01299b8751c7d71c1bc2a6afe900da2c',1,'cytnx::Bond::qnums()']]],
  ['qnums_5fclone_3',['qnums_clone',['../classcytnx_1_1Bond.html#a491a89aa4434d4056dd3e5de7e70e1e7',1,'cytnx::Bond']]],
  ['qr_4',['Qr',['../namespacecytnx_1_1linalg.html#a194461432b34c60984a02569a4d9c903',1,'cytnx::linalg::Qr(const cytnx::UniTensor &amp;Tin, const bool &amp;is_tau=false)'],['../namespacecytnx_1_1linalg.html#af22616b7e35f13b775f2423d19e9ffe7',1,'cytnx::linalg::Qr(const Tensor &amp;Tin, const bool &amp;is_tau=false)']]],
  ['qs_5',['Qs',['../classcytnx_1_1Qs.html#a3f956a47670aa44026633ea9e2633e53',1,'cytnx::Qs::Qs(const cytnx_int64 &amp;e1, const Ts... elems)'],['../classcytnx_1_1Qs.html#ac0109bb5f8c190e4107037bb384291b9',1,'cytnx::Qs::Qs(const std::vector&lt; cytnx_int64 &gt; &amp;qin)']]]
];
