var searchData=
[
  ['hadamard_0',['hadamard',['../namespacecytnx_1_1qgates.html#a37aadd0c317df789f26bad99652d0d3e',1,'cytnx::qgates']]],
  ['histogram_1',['Histogram',['../classcytnx_1_1stat_1_1Histogram.html#a352c96dc863dbe89ed3cd93d9578cf28',1,'cytnx::stat::Histogram']]],
  ['hosvd_2',['Hosvd',['../namespacecytnx_1_1linalg.html#aa1a08b9294b199a6d56f5f5aa2fa58cc',1,'cytnx::linalg::Hosvd(const cytnx::UniTensor &amp;Tin, const std::vector&lt; cytnx_uint64 &gt; &amp;mode, const bool &amp;is_core=true, const bool &amp;is_Ls=false, const std::vector&lt; cytnx_int64 &gt; &amp;trucate_dim=std::vector&lt; cytnx_int64 &gt;())'],['../namespacecytnx_1_1linalg.html#a6c3142f633f9aca2f80bb359b9c58cf5',1,'cytnx::linalg::Hosvd(const Tensor &amp;Tin, const std::vector&lt; cytnx_uint64 &gt; &amp;mode, const bool &amp;is_core=true, const bool &amp;is_Ls=false, const std::vector&lt; cytnx_int64 &gt; &amp;trucate_dim=std::vector&lt; cytnx_int64 &gt;())']]]
];
