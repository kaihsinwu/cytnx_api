 
doxygen_build()
{
    VTAG=$1
    git clone https://github.com/kaihsin/Cytnx.git -b ${VTAG} tmp
    cd tmp
    doxygen docs.doxygen
    mv docs/html ../versions/${VTAG}
    cd ../
    rm -rf tmp
}



#doxygen_build v0.7.3
#doxygen_build v0.7.6
#doxygen_build v0.9.0
#doxygen_build v0.9.1
doxygen_build v0.9.5





