# Cytnx_api


## Requirement:
    - Doxygen
    - sphinx
    - pip install sphinxcontrib-bibtex
    - pip install sphinxbootstrap4theme
    
## Step:
    1. adding different versions in build_doc.sh, this will automatically clone and build Doc for each version 
    2. adding corresponding tag html in home_source/index.rst
    3. "make html" to build main entry for version selection 
    4. push repo
    


