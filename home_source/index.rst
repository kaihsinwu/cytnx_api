
.. image:: Icon_small.png
    :width: 350    

Cytnx API
=================================
    Cytnx is a library design for Quantum physics simulation using GPUs and CPUs.

    Versions:

.. toctree::
    v0.9.3_0.9.5 <https://kaihsinwu.gitlab.io/cytnx_api/v0.9.5/index.html>
    v0.9.1 <https://kaihsinwu.gitlab.io/cytnx_api/v0.9.1/index.html>   
    v0.9.0 <https://kaihsinwu.gitlab.io/cytnx_api/v0.9.0/index.html>   
    v0.7.6 <https://kaihsinwu.gitlab.io/cytnx_api/v0.7.6/index.html>   
    v0.7.3 <https://kaihsinwu.gitlab.io/cytnx_api/v0.7.3/index.html>   


